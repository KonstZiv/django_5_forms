from django.shortcuts import get_object_or_404, render
from django.core.mail import send_mail
from django.http import HttpResponse, HttpRequest
from . import models
from . import forms

def index(request: HttpRequest) -> HttpResponse:
    
    return render(request, 'index.html', {'result': 'уже лекция 6'})

def customer_list(request: HttpRequest) -> HttpResponse:
    customers = models.Customer.objects.all()
    return render(request, 'list.html', {'customers': customers})

def customer_detail(request, surname):
    customer = get_object_or_404(models.Customer, surname=surname)
    return render(request, 'detail.html', {'customer': customer})

def data_share(request:HttpRequest, surname: str) -> HttpResponse:
    # Получаем данные из БД
    customer = get_object_or_404(models.Customer, surname=surname)
    sent = False
    print(f"request.method: {request.method}")
    if request.method == 'POST':
        form = forms.EmailForSubmitData(request.POST)
        if form.is_valid():
            cd =form.cleaned_data
            # отправить письмо
            customer_url = request.build_absolute_uri(customer.get_absolute_url())
            print(customer_url)
            subject = f'info about {customer.surname}'
            message = f"dear {cd['name']}! Info to you:\nname: {customer.name}\nsurname: {customer.surname}\ngender: {customer.gender}\nemail: {customer.email}"
            print(f"cd.name:  {cd['name']}\ncd.email: {cd['email']}")
            send_mail(subject, message, 'test.django.cyberbionic@gmail.com', [cd['email']])
            sent = True
            return render(request, 'share.html', {'customer': customer, 'form': form, 'sent': sent})
    else:
        form = forms.EmailForSubmitData()
        return render(request, 'share.html', {'customer': customer, 'form': form, 'sent': sent})

