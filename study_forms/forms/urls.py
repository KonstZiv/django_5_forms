from django import views
from . import views
from django.urls import path


urlpatterns = [
    path('', views.index, name='index'),
    path('customers/', views.customer_list, name='customers'),
    path('customers/<str:surname>/', views.customer_detail, name='customer_detail'),
    path('customers/<str:surname>/share', views.data_share, name='data_share'),
]
