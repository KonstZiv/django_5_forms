from django.db import models
from django.urls import reverse

class Customer(models.Model):
    class Gender:
        MALE = 'male'
        FEMALE = 'female'
        CHOISE = (
            (MALE, 'чоловік'),
            (FEMALE, 'жінка')
        )
    
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100, unique=True)
    email = models.EmailField()
    gender = models.CharField(max_length=6, choices=Gender.CHOISE)

    def get_absolute_url(self):
        return reverse('customer_detail', args=[self.surname])


    
